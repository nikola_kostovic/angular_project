import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ChefLifeComponent } from './chef_life_page/chef_life.component';
import { IndexPageComponent } from './index_page/indexPage.component';
import { NavbarComponent } from './navbar/navbar.component';
import { VideoGalleryComponent } from './index_page/video-gallery/video-gallery.component';
import { SharedModule } from './shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';
import { PodcastPageComponent } from './podcast_page/podcast-page.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ClVideoLandingComponent } from './chef_life_video_landing_page/cl-video-landing.component';
import { MyStoryComponent } from './my_story_page/my-story.component';
import { PodcastLandingComponent } from './podcast_landing_page/podcast-landing.component';
import { NavDropdownComponent } from './navbar/nav_dropdown/nav-dropdown.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    IndexPageComponent,
    ChefLifeComponent,
    VideoGalleryComponent,
    PodcastPageComponent,
    ClVideoLandingComponent,
    MyStoryComponent,
    PodcastLandingComponent,
    NavDropdownComponent


  ],
  imports: [
    BrowserModule,
    SharedModule,
    RouterModule.forRoot([
      { 
        path: "index",
        component: IndexPageComponent
      },
      { path: "chef_life", component: ChefLifeComponent },
      { path: "podcast", component: PodcastPageComponent, data: {some_data: 'some value'}},
      { path: "chef_life_video/:id", component: ClVideoLandingComponent },
      { path: "podcast/:id", component: PodcastLandingComponent },
      { path: "my_story", component: MyStoryComponent },
      { path: '', redirectTo: "index", pathMatch: 'full' }
    ], {scrollPositionRestoration: 'enabled'}),
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
