import { Component, OnInit } from "@angular/core";
import { DataService } from "../data.service";
import { IVideos } from "../shared/video/video";

@Component({
    templateUrl: "chef_life.component.html",
    styleUrls: ["./chef_life.component.scss"]
})

export class ChefLifeComponent implements OnInit {

    index_footer_img:string = "/assets/images/chef_life_footer.png";
    videos: IVideos[]=[];
    private _videoService;
  
    constructor(videoService: DataService) {
      this._videoService = videoService;
     }
     ngOnInit(): void {
        this.videos = this._videoService.getVideos();
        console.log(this.videos);
      }
    
}