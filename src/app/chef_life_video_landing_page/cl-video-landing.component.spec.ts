import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClVideoLandingComponent } from './cl-video-landing.component';

describe('ClVideoLandingComponent', () => {
  let component: ClVideoLandingComponent;
  let fixture: ComponentFixture<ClVideoLandingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClVideoLandingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClVideoLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
