import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-cl-video-landing',
  templateUrl: './cl-video-landing.component.html',
  styleUrls: ['./cl-video-landing.component.scss']
})
export class ClVideoLandingComponent implements OnInit {

  title= "";
  description= "";
  id;
  videos :any;
  constructor(route: ActivatedRoute, videoservice: DataService) { 
    this.videos = videoservice.getVideos();
    this.id= route.snapshot.params['id'];
  }

  ngOnInit(): void {
    console.log();
    this.title = this.videos[this.id-1]['videoTitle']
    this.description = this.videos[this.id-1]['videoDescription2']

  }

  shiftRight =() =>{
console.log("left");

    var deleted = this.videos.shift();
    this.videos.push(deleted);
    
   }
  shiftLeft =() =>{
   
    var deleted = this.videos.pop();
    this.videos.unshift(deleted);
    
   }

}
