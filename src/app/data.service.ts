import { Injectable } from "@angular/core";
import { IVideos } from "./shared/video/video";
import * as data from "../assets/videos.json"
import * as podcast from "../assets/podcasts.json"

@Injectable({
    providedIn: "root"
})

export class DataService{

    getVideos(): IVideos[] {
        let vids: any = (data as any).default;
        return vids;
               
    }
    getPodcasts(): any {
        let podcasts_array: any = (podcast as any).default;
        return podcasts_array;
               
    }
}