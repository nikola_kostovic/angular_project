import { Component } from "@angular/core";

import { modalService } from "../modals.service";

@Component({
  templateUrl: "indexPage.component.html",
  styleUrls: ["indexPage.component.scss"]

})

export class IndexPageComponent {
  index_footer_img: string = "/assets/images/index_footer.png";

  private _modal: any;

  constructor(modalservice: modalService) {
    this._modal = modalservice;
  }
  open = (str: string) => { this._modal.open(str) }

  scrollToSection(): void {

    document.getElementById('section-b')?.scrollIntoView({    
      block: 'start',
      inline: 'nearest',
      behavior: "smooth" })
  }
}


