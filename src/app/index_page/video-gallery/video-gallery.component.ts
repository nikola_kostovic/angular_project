import { Component, OnInit } from '@angular/core';
import { IVideos } from '../../shared/video/video';
import { DataService } from '../../data.service';

@Component({
  selector: 'app-video-gallery',
  templateUrl: './video-gallery.component.html',
  styleUrls: ['./video-gallery.component.scss']
})
export class VideoGalleryComponent implements OnInit {

  videos: IVideos[]=[];
  private _videoService;

  constructor(videoService: DataService) {
    this._videoService = videoService;
   }

  ngOnInit(): void {
    this.videos = this._videoService.getVideos();
    console.log(this.videos);
  }

}
