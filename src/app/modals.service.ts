import { Injectable } from "@angular/core";
import { ModalDismissReasons, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ModalComponent } from "./shared/modal/modal.component";
import { ModalConfirmationComponent } from "./shared/modalConfirmation/modal-confirmation.component";

@Injectable({
  providedIn: "root"
})

export class modalService {

  constructor(private modalService: NgbModal) { }
  closeResult = "";
  result : any;
  
  open(source: string) {
    if (source == "freePlaybook") {

      const modalRef = this.modalService.open(ModalComponent);
      modalRef.componentInstance.text = 'GET MY FREE PLAYBOOK. 10 WAYS TO BUILD YOUR COOKING SKILLS IN YOUR KITCHEN.';
      modalRef.componentInstance.modal = 1;
      modalRef.result.then((result) => {
        this.closeResult = `${result}`;
      });
    }
    else if (source == "subscribe") {
      const modalRef = this.modalService.open(ModalComponent);
      modalRef.componentInstance.text = 'JOIN ME & STAY IN THE LOOP ON EVERYTHING!';
      modalRef.componentInstance.modal = 2;
      modalRef.result.then((result) => {
        this.closeResult = `${result}`;
      });
    }
    else if (source == "ty") {
      const modalRef = this.modalService.open(ModalConfirmationComponent);

      modalRef.componentInstance.text = 'THANK YOU FOR SIGNING UP.';
      modalRef.componentInstance.message = 'YOU WILL BE RECEIVING MY PLAYBOOK DIRECTLY TO YOUR EMAIL.';
      modalRef.result.then((result) => {
        this.closeResult = `${result}`;
      });
    }
    else if (source == "tys") {
      const modalRef = this.modalService.open(ModalConfirmationComponent);
      modalRef.componentInstance.text = 'THANK YOU FOR SUBSCRIBING';
 
      modalRef.result.then((result) => {
        this.closeResult = `${result}`;
      });
    
    }
   
  }

  

}

