import { Component } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";

@Component({
    selector: "navbar",
    templateUrl: "./navbar.component.html",
    styleUrls: ["./navbar.component.scss"]
})

export class NavbarComponent {



    constructor(private router: Router) {
        router.events.subscribe((val) => {

            console.log(val instanceof NavigationEnd)
            this.nav_dropdown = false;
            this.icon = true
            this.close = false
        });
    }
    nav_dropdown: boolean = false;
    icon: boolean = true;
    close: boolean = false;

    show_hide = () => {
        this.nav_dropdown = !this.nav_dropdown;
        this.icon = !this.icon;
        this.close = !this.close
    }

}