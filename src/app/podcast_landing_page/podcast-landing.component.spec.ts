import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PodcastLandingComponent } from './podcast-landing.component';

describe('PodcastLandingComponent', () => {
  let component: PodcastLandingComponent;
  let fixture: ComponentFixture<PodcastLandingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PodcastLandingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PodcastLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
