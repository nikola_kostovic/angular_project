import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-podcast-page',
  templateUrl: './podcast-page.component.html',
  styleUrls: ['./podcast-page.component.scss']
})
export class PodcastPageComponent implements OnInit {

  podcast_footer_img = "assets/images/pexels.png";
  podcasts: any = [];
  private _dataService;
  route;
  constructor(dataservce : DataService, route: ActivatedRoute) {
    this._dataService = dataservce;
    this.route = route;
   }

  ngOnInit(): void {
    this.podcasts = this._dataService.getPodcasts();
 
    this.route
    .data
    .subscribe();
  }

}
