import { Component, ElementRef, HostBinding, Input, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { modalService } from 'src/app/modals.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  @Input() imgURL: string | any;
  private _modal;

  footer_type: boolean = true;

  constructor(modalservice: modalService, private el: ElementRef) {
    this._modal = modalservice;
  }

  open = (str: string) => { this._modal.open(str) }

  ngOnInit(): void {

    if (this.imgURL == undefined) {
      this.footer_type = false
      let footer = document.getElementById('footer');
      let footer_cnt = document.getElementById('footerContainer');
      footer?.classList.add("grey-background");
      footer_cnt?.classList.remove('container-bg');

    }

  }

}
