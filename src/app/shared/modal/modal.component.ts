import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { modalService } from 'src/app/modals.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit, OnDestroy {

  closeResult = '';
  private _modal_service;
  @Input() text: string;
  @Input() modal: number | undefined;

  constructor(public activeModal: NgbActiveModal, modal_service: modalService) {
    this.text = "";
    this._modal_service = modal_service;
  }

  ngOnDestroy(): void {

    if (this._modal_service.closeResult != "submit") {

      document.body.style.overflow = "auto";
    }

  }

  open = (str: string) => {
 
    if (this.modal == 1) {
      this._modal_service.open(str)
      console.log(this.modal+ str)
    }
    else
    {
      this._modal_service.open(str+'s')
      console.log(this.modal)
    }
  }

  ngOnInit(): void {

    document.body.style.overflow = "hidden";

  }

}
