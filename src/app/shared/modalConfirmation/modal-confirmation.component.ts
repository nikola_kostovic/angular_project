import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-confirm',
  templateUrl: './modal-confirmation.component.html',
  styleUrls: ['./modal-confirmation.component.scss']
})
export class ModalConfirmationComponent  implements OnInit ,OnDestroy {

  @Input() text: string | undefined;
  @Input() message: string | undefined;

  constructor(public activeModal: NgbActiveModal){

  }

  ngOnDestroy(): void {
    document.body.style.overflow = "auto";
    console.log("destr sec")
  }
  ngOnInit(): void {
    document.body.style.overflow = "hidden";
    console.log("init second")
  }

}
