import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-podcast-card',
  templateUrl: './podcast-card.component.html',
  styleUrls: ['./podcast-card.component.scss']
  

})
export class PodcastCardComponent implements OnInit {

  @Input() Podcast_ID: number | undefined;
  @Input() Podcast_Title: string | undefined;
  @Input() Podcast_Date: string | undefined;
  @Input() Podcast_Content: string | undefined;
  @Input() Podcast_ImgUrl: string | undefined;
  
  constructor() { }

  ngOnInit(): void {
 
    console.log(this.Podcast_ID,this.Podcast_Date, this.Podcast_ImgUrl)
  }

}
