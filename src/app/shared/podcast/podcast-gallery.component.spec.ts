import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PodcastGalleryComponent } from './podcast-gallery.component';

describe('PodcastGalleryComponent', () => {
  let component: PodcastGalleryComponent;
  let fixture: ComponentFixture<PodcastGalleryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PodcastGalleryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PodcastGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
