import { animate } from '@angular/animations';
import { Component, Input, OnInit } from '@angular/core';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-podcast-gallery',
  templateUrl: './podcast-gallery.component.html',
  styleUrls: ['./podcast-gallery.component.scss']
})
export class PodcastGalleryComponent implements OnInit {

  podcasts: any = [];

  private _podcastService;
  constructor(podcastService: DataService) {
    this._podcastService = podcastService;
    
    this.podcasts = this._podcastService.getPodcasts();
    console.log(this.podcasts)
  }
 shiftRight =() =>{

   var deleted = this.podcasts.shift();
   this.podcasts.push(deleted);

   var cards = document.getElementsByName('card');
   console.log(cards[1]);
   
   
  }
 shiftLeft =() =>{
  
   var deleted = this.podcasts.pop();
   this.podcasts.unshift(deleted);

   
  }
  ngOnInit(): void {
    
  }



}
