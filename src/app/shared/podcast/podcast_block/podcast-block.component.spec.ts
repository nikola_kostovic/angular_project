import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PodcastBlockComponent } from './podcast-block.component';

describe('PodcastBlockComponent', () => {
  let component: PodcastBlockComponent;
  let fixture: ComponentFixture<PodcastBlockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PodcastBlockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PodcastBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
