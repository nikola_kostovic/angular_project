import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-podcast-block',
  templateUrl: './podcast-block.component.html',
  styleUrls: ['./podcast-block.component.scss']
})
export class PodcastBlockComponent implements OnInit {

  @Input() Podcast_ID: number | undefined;
  @Input() Podcast_Title: string | undefined;
  @Input() Podcast_Date: string | undefined;
  @Input() Podcast_Content: string | undefined;
  @Input() Podcast_ImgUrl: string | undefined;
  

  constructor() { }

  ngOnInit(): void {
  }

}
