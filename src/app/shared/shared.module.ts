import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideoCardComponent } from './video/video-card.component';
import { PodcastGalleryComponent } from './podcast/podcast-gallery.component';
import { PodcastCardComponent } from './podcast/podcast-card.component';
import { FooterComponent } from './footer/footer.component';
import { PodcastBlockComponent } from './podcast/podcast_block/podcast-block.component';
import { ModalComponent } from './modal/modal.component';
import {  RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalConfirmationComponent } from './modalConfirmation/modal-confirmation.component';

@NgModule({
  declarations: [ 
    VideoCardComponent,
    PodcastGalleryComponent,
    PodcastCardComponent,
    FooterComponent,
    PodcastBlockComponent,
    ModalComponent,
    ModalConfirmationComponent
 
  ],

  imports: [
    CommonModule,
    RouterModule,
    NgbModule,
    
  ],
  exports:[
    CommonModule,
    VideoCardComponent,
    PodcastGalleryComponent,
    FooterComponent,
    PodcastBlockComponent,
    ModalComponent,

  ]
})
export class SharedModule { }
