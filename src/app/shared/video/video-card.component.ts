import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'app-video-card',
  templateUrl: './video-card.component.html',
  styleUrls: ['./video-card.component.scss']
})
export class VideoCardComponent implements OnInit {

  @Input() videoId: number | undefined;
  @Input() videoTitle: string | undefined;
  @Input() videoDescription: string | undefined;
  @Input() videoUrl: string | undefined;
  

  ngOnInit(): void {
  }

}
