export interface IVideos{
  videoId: number;
  videoTitle: string;
  videoDescription: string;
  videoDescription2: string;
  videoImgUrl: string;   
}